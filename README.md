# Gitlab Group Members

Do you need a list of all members of your groups and the projects in them?
This simple web page and Javascript program generates a useful report.

Load GitlabGroupMembers.html in your favorite browser,
enter a list of group names,
and your Gitlab API Access Token.

Click Generate.

You'll get a report of all users with access to your groups
and the repositories in them.

This may be useful for auditing access, or for compliance purposes.
That's why I created it.

(This won't work in Internet Explorer.)
